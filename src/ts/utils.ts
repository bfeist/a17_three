export function timeStrCompToSeconds(timeStr: string) {
  var sign = timeStr.substr(0, 1);
  var hours = parseInt(timeStr.substr(0, 3));
  var minutes = parseInt(timeStr.substr(3, 2));
  var seconds = parseInt(timeStr.substr(5, 2));
  var signToggle = sign == "-" ? -1 : 1;
  var totalSeconds = Math.round(signToggle * (Math.abs(hours) * 60 * 60 + minutes * 60 + seconds));
  //if (totalSeconds > 230400)
  //    totalSeconds -= 9600;
  return totalSeconds;
}

export function secondsToTimeStr(totalSeconds: number) {
  var hours = Math.abs(Math.round(totalSeconds / 3600));
  var minutes = (Math.abs(Math.round(totalSeconds / 60)) % 60) % 60;
  var seconds = Math.abs(Math.round(totalSeconds)) % 60;
  seconds = Math.floor(seconds);
  var timeStr = padZeros(hours, 3) + ":" + padZeros(minutes, 2) + ":" + padZeros(seconds, 2);
  if (totalSeconds < 0) {
    timeStr = "-" + timeStr.substr(1); //change timeStr to negative, replacing leading zero in hours with "-"
  }
  return timeStr;
}

export function padZeros(num: number, size: number) {
  let s = num + "";
  while (s.length < size) s = "0" + s;
  return s;
}
