import * as THREE from "three";
import { OrbitControls } from "three-orbitcontrols-ts";
import { getPhotoData, getUtteranceData, getVideoURLData } from "./ajax";
import { MissionData } from "./interfaces";
import { timeStrCompToSeconds, secondsToTimeStr } from "./utils";

const gMissionData: MissionData = {
  videoURLData: [],
  photoData: [],
  utteranceData: [],
};

function drawThree() {
  /**
   * Scene setup
   */
  const scene: THREE.Scene = new THREE.Scene();
  const renderer: THREE.WebGLRenderer = new THREE.WebGLRenderer({
    antialias: true,
  });
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);
  function render() {
    renderer.render(scene, camera);
  }
  const camera: THREE.PerspectiveCamera = new THREE.PerspectiveCamera(
    25,
    window.innerWidth / window.innerHeight,
    0.001,
    100000
  );
  camera.position.z = -50.5;
  camera.position.y = 0;
  camera.position.x = 0;
  camera.rotateY(THREE.MathUtils.degToRad(-180));
  const controls = new OrbitControls(camera, renderer.domElement);

  /**
   * Scene Objects
   */
  // const boxGeometry: THREE.BoxGeometry = new THREE.BoxGeometry();
  // const boxMaterial: THREE.MeshBasicMaterial = new THREE.MeshPhongMaterial({
  //   color: 0x00ff00,
  //   wireframe: true,
  // });

  // const cube: THREE.Mesh = new THREE.Mesh(boxGeometry, boxMaterial);
  // scene.add(cube);

  const bgLoader = new THREE.TextureLoader();
  const bgTexture = bgLoader.load("img/starmap_8k.jpg", function () {
    var rt = new THREE.WebGLCubeRenderTarget(bgTexture.image.height);
    rt.fromEquirectangularTexture(renderer, bgTexture);
    scene.background = rt;
  });

  // var bgGeometry = new THREE.SphereGeometry(500, 60, 40);
  // bgGeometry.scale(-10, 10, 10);

  // var bgMaterial = new THREE.MeshBasicMaterial({
  //   map: new THREE.TextureLoader().load("img/starmap_8k.jpg"),
  // });

  // var bgMesh = new THREE.Mesh(bgGeometry, bgMaterial);
  // scene.add(bgMesh);

  const timelineCompressionFactor = 200;

  let wallNum = 0;
  for (let i = 0; i < gMissionData.photoData.length; i++) {
    // for (let i = 0; i < 150; i++) {
    const photo = gMissionData.photoData[i];

    const photoSeconds = timeStrCompToSeconds(photo.timeStr);
    const z = photoSeconds / timelineCompressionFactor;

    const geometry = new THREE.BoxGeometry(0.001, 10, 10);
    const photoFolder = photo.magNum === "" ? "supporting/100/" : "flight/100/AS17-";
    const texture = new THREE.TextureLoader().load(
      `https://keycdnmediado.apolloinrealtime.org/A17/images/${photoFolder}${photo.filenamePrefix}.jpg`
    );
    const material: THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({
      color: 0xffffff,
      wireframe: false,
      map: texture,
      depthWrite: false,
    });
    const tick: THREE.Mesh = new THREE.Mesh(geometry, material);
    tick.position.z = z;

    switch (wallNum) {
      case 0:
        tick.position.x = -5;
        break;
      case 1:
        tick.position.y = 5;
        tick.rotateZ(THREE.MathUtils.degToRad(90));
        break;
      case 2:
        tick.position.x = 5;
        break;
      case 3:
        tick.position.y = -5;
        tick.rotateZ(THREE.MathUtils.degToRad(90));
        break;
    }
    wallNum = wallNum < 3 ? wallNum + 1 : 0;

    // tick.position.x = nudge;
    // nudge -= 0.001;
    scene.add(tick);
  }

  /**
   * Animation and resizing
   */
  const timeElement = document.getElementById("time");
  var animate = function () {
    requestAnimationFrame(animate);

    //   cube.rotation.x += 0.001;
    //   cube.rotation.y += 0.001;

    if (camera.position.z > 4500) camera.position.z = -50;

    camera.position.z += 0.5;
    controls.target = new THREE.Vector3(0, 0, camera.position.z + 5);
    // bgMesh.position.set(camera.position.x, camera.position.y, camera.position.z);

    const currentSeconds = Math.round(camera.position.z * timelineCompressionFactor);
    if (timeElement) {
      timeElement.innerHTML = secondsToTimeStr(currentSeconds);
    }

    controls.update();

    render();
  };

  animate();

  window.addEventListener("resize", onWindowResize, false);
  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    render();
  }
}

async function main() {
  gMissionData.videoURLData = await getVideoURLData();
  gMissionData.photoData = await getPhotoData();
  gMissionData.utteranceData = await getUtteranceData();

  drawThree();
}

main();
